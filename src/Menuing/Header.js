import React from 'react';

class Header extends React.Component {
    constructor(props) {
        super(props);
        if(this.props.loadouts === undefined) {
            throw new Error("loadoutless headers may not apply");
        }

        this.state = {
            selected: this.props.selected
                && this.props.loadouts[this.props.selected]
                ? this.props.selected
                : undefined
        };
    }

    isSelected(key) {
        return this.state.selected !== undefined
            && this.state.selected === key;
    }

    renderOption(key) {
        let capName = key.charAt(0).toUpperCase() + key.slice(1);
        return this.isSelected(key)
            ? <option key={key} value={key} selected>{capName}</option>
        : <option key={key} value={key}>{capName}</option>;
    }

    render() {
        return (<header>
                  <h1> Sweepey Todd</h1>
                  <button
                    onClick={e => this.props.handleCustomRulesSelect(e)}
                    className="headerButton button"
                  >
                    Custom...
                  </button>
                  <select
                    className="difficultySelector button"
                    onChange={e => this.props.handleLoadout(e.target.value)}
                  >
                    { this.props.loadouts.map(e => this.renderOption(e)) }
                  </select>
                  <button
                    onClick={e => this.props.handleNewGame()}
                    className="headerButton button"
                  >
                    New Game
                  </button>
                </header>);
    }
}

export default Header;
