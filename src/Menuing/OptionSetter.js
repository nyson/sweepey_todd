import React from 'react';

class OptionSetter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            width:  this.props.width      || 20,
            height: this.props.height     || 30,
            slider: this.props.percentage || 0.1
        };

        this.changeWidth.bind(this);
        this.changeHeight.bind(this);
        this.changeSlide.bind(this);
    }

    setLoadout(loadout) {
        this.setState({
            width:  loadout.width,
            height: loadout.height,
            slider: loadout.percentage
        });
    }

    render() {
        return (<fieldset className="boardOptions">
                  <legend>Options</legend>
                  <LabelledInput
                    name="width"
                    label="Width: "
                    type="number"
                    value={this.state.width}
                    onChange={e => this.changeWidth(e)}
                  />
                  <br/>
                  <br />
                  <LabelledInput
                    name="height"
                    label="Height: "
                    type="number"
                    value={this.state.height}
                    onChange={e => this.changeHeight(e)}
                  />
                  <br/>
                  <Slider
                    name="copperPercentage"
                    label="Percentage of coppers in field"
                    type="range"
                    min="0.00"
                    max="1.00"
                    step="0.01"
                    width={this.state.width}
                    height={this.state.height}
                    value={this.state.slider}
                    onChange={e => this.changeSlide(e)}
                  />
                  <button className="submitOptions button"
                          onClick={e => this.submit(e)}>
                    Submit
                  </button>
                </fieldset>
               );
    }

    submit(e){
        this.props.handleSubmitOptions(
            this.state.slider,
            this.state.width,
            this.state.height
        );
    }

    changeWidth(e){
        this.setState({
            width: e.target.value
        });
    }

    changeHeight(e){
        this.setState({
            height: e.target.value
        });
    }

    changeSlide(e){
        this.setState({
            slider: e.target.value
        });
    }
}

class LabelledInput extends React.Component {
    render() {
        return (<>
                  <label
                    className="labelledInputLabel"
                    htmlFor={this.props.name}
                  >
                    {this.props.label}
                  </label>
                  <input className="labelledInputInput"
                         type={this.props.type}
                         name={this.props.name}
                         min={this.props.min}
                         max={this.props.max}
                         step={this.props.step}
                         value={this.props.value}
                         onChange={this.props.onChange}
                  />
                </>);
    }
}

class Slider extends React.Component {
    render() {
        return (<div className="sliderWrapper">
                  <label className="labelledInputLabel cops"
                         htmlFor={this.props.name}>
                    {this.label}
                  </label>
                  <input type="range"
                         className="labelledInputInput"
                         name={this.props.name}
                         min={this.props.min}
                         max={this.props.max}
                         step={this.props.step}
                         value={this.props.value}
                         onChange={this.props.onChange}
                  />
                </div>);
    }

    get label() {
        const [w,h] = [this.props.width, this.props.height];
        return (<>
                  <span>{Math.ceil(this.props.value*w*h) + "/" + (h*w)}</span>
                  <br/>
                  <span>cops</span>
                </>);
    }

    amount(perc, w,h) {
        return Math.ceil(perc * w * h) + "of" + w*h;
    }
}

export default OptionSetter;
