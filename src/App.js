import React from 'react';
import './App.css';
import SweepBoard from './SweepBoard/SweepBoard.js';
import Header from './Menuing/Header.js';
import OptionSetter from './Menuing/OptionSetter.js';

class App extends React.Component {
    availableLoadouts = {
        'easy': {
            width: 20,
            height: 10,
            percentage: 0.1
        },
        'medium': {
            width: 50,
            height: 20,
            percentage: 0.15
        },
        'hard': {
            width: 80,
            height: 40,
            percentage: 0.20
        },
    }

    constructor(props) {
        super(props);
        this.onSubmitOptions.bind(this);
        this.state = this.stateFromLoadout('easy');
    }

    stateFromLoadout(lkey) {
        const l = this.availableLoadouts[lkey];

        return {
            width: l.width,
            height: l.height,
            copperPercentage: l.percentage,
            seed: Math.ceil(Math.random()*1000000000),
            view: 'game'
        };
    }

    setView(view) {
        this.setState({view: view});
    }

    setLoadout(key) {
        const loadout = this.availableLoadouts[key] !== undefined
              ? key : 'easy';

        this.setState(this.stateFromLoadout(loadout));
    }

    onSubmitOptions(p,w,h) {
        this.setState({
            copperPercentage: parseFloat(p),
            width: parseInt(w),
            height: parseInt(h),
            view: 'game'
        });
        console.debug(this.state, p, w, h);
    }

    onNewGame() {
        console.debug("starting new game");
        this.setState({seed: Math.ceil(Math.random()*1000000000)});
    }

    render() {
        return (
            <>
              <Header handleLoadout={key => this.setLoadout(key)}
                      handleNewGame={() => this.onNewGame()}
                      loadouts={Object.keys(this.availableLoadouts)}
                      handleCustomRulesSelect={e => this.setView('customRules')}
              />
              {this.renderCenter()}
            </>
        );
    }

    renderCenter() {
        console.debug("renders center: ", this.state.view);
        switch(this.state.view) {
        default:
        case 'game':
            return (<SweepBoard
                      width={this.state.width}
                      height={this.state.height}
                      copperPercentage={this.state.copperPercentage}
                      seed={this.state.seed}
                    />);

        case 'customRules':
            console.debug("custom rules");
            return (<OptionSetter
                      handleSubmitOptions={(p, w, h) => this.onSubmitOptions(p, w, h)}
                      width={this.state.width}
                      height={this.state.height}
                      percentage={this.state.copperPercentage}
                    />);
        }
    }

    setCustomRules(w,h,p) {
        this.onSubmitOptions(p, w, h);
        this.setState({view: 'game'});
    }
}

export default App;
