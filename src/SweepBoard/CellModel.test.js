import CellModel, {Visibility, visibility} from './CellModel.js';

describe('creation', () => {
    const c = new CellModel(true, 10, 5);

    it('created cell has correct position', () => {
        expect(c.pos)
            .toEqual({x: 10, y: 5});
    });

    it('created cell has correct value', () => {
        expect(c.isCop)
            .toEqual(true);
    });

    it('created cell has correct visibility', () => {
        expect(c.visibility)
            .toEqual(visibility.HIDDEN);
    });
});

describe('mutation', () => {
    describe('adjacents', () => {
        const withCop = new CellModel(true, 10, 5);
        const notCop = new CellModel(false, 10, 5);

        it('can set adjacents', () => {
            notCop.adjacents = 5;
            expect(notCop.adjacents).toEqual(5);
        });

        it("can't set adjacents if it's a cop", () => {
            expect(() => withCop.adjacents = 5)
            .toThrow("A cop doesn't know about adjacent cops!");
        });

        it("can't set more adjacents than there are adjacent blocks", () => {
            expect(() => notCop.adjacents = 9)
                .toThrow("9 is an invalid number of adjacent cops!"
                         + " Should be between 1 and 8 (inclusive)");
        });
    });

    describe('flagging', () => {
        it("can make a cell flagged", () => {
            const withCop = new CellModel(true, 10, 5);

            expect(withCop.visibility)
                .toEqual(visibility.HIDDEN);

            withCop.toggleFlag();

            expect(withCop.visibility)
                .toEqual(visibility.FLAG);
        });

        it("can unflag flagged cells", () => {
            const withCop = new CellModel(true, 10, 5);

            expect(withCop.visibility)
                .toEqual(visibility.HIDDEN);

            withCop.toggleFlag();

            expect(withCop.visibility)
                .toEqual(visibility.FLAG);

            withCop.toggleFlag();

            expect(withCop.visibility)
                .toEqual(visibility.HIDDEN);
        });
    });

    describe('visibility', () => {
        it("cell objects are not equal", () => {

            expect(Visibility.HIDDEN)
                .not.toEqual(Visibility.FLAG);
            expect(Visibility.FLAG)
                .not.toEqual(Visibility.VISIBLE);
            expect(Visibility.VISIBLE)
                .not.toEqual(Visibility.HIDDEN);

        });
        const withCop = new CellModel(true, 10, 5);
        const notCop = new CellModel(false, 10, 5);

        it("can make a cell visible", () => {
            expect(notCop.visibility)
                .toEqual(visibility.HIDDEN);

            notCop.makeVisible();

            expect(notCop.visibility)
                .toEqual(visibility.VISIBLE);
        });

        it("will throw when cell is in a bad state during reveal", () => {
            const c = new CellModel(false, 0, 0);
            c._visibility = "TORSTEN";
            expect(() => c.makeVisible())
                .toThrow("Unrecognized visibility value");
        });

        it("can't make a flag visibile", () => {
            withCop.toggleFlag();

            expect(withCop.visibility)
                .toEqual(visibility.FLAG);

            withCop.makeVisible();

            expect(withCop.visibility)
                .toEqual(visibility.FLAG);
        });

        it("will keep a visible cell visible", () => {
            notCop.makeVisible();

            expect(notCop.visibility)
                .toEqual(visibility.VISIBLE);

            notCop.makeVisible();

            expect(notCop.visibility)
                .toEqual(visibility.VISIBLE);
        });

        it("can't make a visible cell flagged", () => {
            const c = new CellModel(false, 0, 0);
            c.makeVisible();
            expect(() => c.toggleFlag())
                .toThrow("Can only toggle flags on flags and hidden cells");
        });

        it("throws when an invalid visibility is flagged", () => {
            const c = new CellModel(false, 0, 0);
            c._visibility = "terke";
            expect(() => c.toggleFlag())
                .toThrow("Can only toggle flags on flags and hidden cells");
        });
    });

    describe('html getters', () => {
        describe('classname', () => {
            it('handles hiddens', () => {
                expect(new CellModel(false, 0, 0).className)
                    .toEqual("cell-hidden");
            });

            it('handles flags', () => {
                const c = new CellModel(false, 0, 0);
                c.toggleFlag();
                expect(c.className)
                    .toEqual("cell-flag");
            });

            it('handles coppers', () => {
                const c = new CellModel(true, 0, 0);
                c.makeVisible();
                expect(c.className)
                    .toEqual("cell-copper");
            });

            it('handles nothings', () => {
                const c = new CellModel(false, 0, 0);
                c._adjacents = 0;
                c.makeVisible();
                expect(c.className)
                    .toEqual("cell-nothing");
            });

            it('handles adjacents', () => {
                const c = new CellModel(false, 0, 0);
                c._adjacents = 8;
                c.makeVisible();
                expect(c.className)
                    .toEqual("cell-nothing cell-adjacents-8");
            });
        });

        describe("text", () => {
            it("shows nothing for nothing", () => {
                expect(new CellModel(true, 0, 0).text).toEqual("");
            });

            it("shows adjacents for visible adjacents", () => {
                const c = new CellModel(false, 0, 0);
                c.adjacents = 8;
                c.makeVisible();
                expect(c.text).toEqual("8");
            });

            it("shows flags", () => {
                const c = new CellModel(true, 0, 0);
                c.toggleFlag();
                expect(c.text).toEqual("F");
            });
        });
    });

    describe("fromString", () => {
        it("can convert a string to cell", () => {
            ["C", "/", "8"].forEach(val => ["£", "_", "F"].forEach(vis => {
                expect(CellModel.mkCell(val + vis, 0, 0).toString())
                    .toEqual(val + vis);
            }));
        });

        it("throws on invalid visibility", () => {
            const c = new CellModel(true, 0, 0);
            c._visibility = "TORKEL";
            expect(() => c.toString()).toThrow("Invalid visibility value: TORKEL");
        });
    });

    describe("pos", () => {
        const c = new CellModel(false, 0, 0);
        it("sets correctly", () => {
            c.pos = {x: 1, y: 1};
            expect(c.pos).toEqual({x: 1, y: 1});
        });

        it("throws on NaN", () => {
            const badNum = "Harry Potter and the Portrait That"
                  + " Looked Like a Large Pile Of Ash (read by SungWon Cho)";
            expect(() => c.pos = {
                x: 0,
                y: badNum
            }).toThrow("y value is not a number or less than zero: '" + badNum + "'");
        });
    });
});
