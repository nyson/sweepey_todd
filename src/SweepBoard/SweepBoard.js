import React      from 'react';
import BoardModel from './BoardModel.js';
import Cell       from './Cell.js';
import                 './SweepBoard.css';

class SweepBoard extends React.Component {
    constructor(props) {
        super(props);

        if(isNaN(this.props.height)
           || isNaN(this.props.width)
           || this.props.copperPercentage < 0
           || this.props.copperPercentage > 1){
            throw new Error("Bad input data: (w,h,cp) = ("
                            + this.props.width + ", "
                            + this.props.height + ", "
                            + this.props.copperPercentage + ")");
        }

        const board = this.freshBoard();

        this.state = {
            gamestate: board.gamestate,
            board: board
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.height !== this.props.height
           || prevProps.width !== this.props.width
           || prevProps.copperPercentage !== this.props.copperPercentage
           || prevProps.seed !== this.props.seed) {
            const board = this.freshBoard();

            this.setState({
                gamestate: board.gamestate,
                board: board
            });
        }
    }

    freshBoard() {
        return new BoardModel(
            this.props.width,
            this.props.height,
            this.props.copperPercentage
        );
    }

    handleCellClick(x, y) {
        this.state.board.pop(x,y);

        this.setState({
            gamestate: this.state.board.gamestate,
            board: this.state.board
        });
    }

    handleFlag(x,y) {
        this.state.board.flag(x,y);

        this.setState({
            gamestate: this.state.board.gamestate,
            board: this.state.board
        });
    }

    render() {
        const cells = this.state.board.getGrid();

        return (
            <table className="sweepboard">
              <tbody>
                { cells.map((row, rKey) => (
                    <tr key={rKey} >
                      { row.map(cell =>
                                <Cell
                                  key={cell.pos.y}
                                  model={cell}
                                  text={cell.text}
                                  className={cell.className}
                                  posX={cell.pos.x}
                                  posY={cell.pos.y}
                                  onCellClick={(x,y) =>
                                               this.handleCellClick(x,y)}
                                  handleFlag={(x,y) =>
                                              this.handleFlag(x,y)}
                                />)
                      }
                    </tr>))
                }
              </tbody>
            </table>);
    }
}

export default SweepBoard;
