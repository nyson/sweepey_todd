import SweepBoard from './SweepBoard.js';
import React from 'react';
import ReactDOM from 'react-dom';


it('can render', () => {
    var e = document.createElement("div");
    e.setAttribute("id", "root");
    document.body.appendChild(e);
    ReactDOM.render(
        <SweepBoard
          width="30"
          height="20"
          copperPercentage="0.1"
        />,
        document.getElementById("root")
    );
});
