import BoardModel, {gamestate} from './BoardModel.js';
import CellModel, {visibility} from './CellModel.js';

describe('Board Model', () => {
    let board = new BoardModel(2,2,0.25);
    describe('gamestate', () => {
        it('loses the game if a copper is visible', () => {
            let board = new BoardModel(2,2,1);

            board.pop(1,1);

            expect(board.gamestate)
                .toEqual(gamestate.LOST);
        });

        it('wins the game if only coppers are hidden', () => {
            let board = new BoardModel(2,2,1);
            expect(board.gamestate)
                .toEqual(gamestate.WON);
        });

        it('wins the game if only coppers are flagged', () => {
            expect(BoardModel.fromStrings(
                ["CF CF",
                 "CF 3F"]).gamestate)
                .toEqual(gamestate.ACTIVE);
        });

        it("wins the game wether cops are flagged", () => {
            expect(BoardModel.fromStrings(
                ["CH CH",
                 "CH 3_"]).gamestate)
                .toEqual(gamestate.WON);
        });

        it("a new game isn't won by default", () => {
            expect(new BoardModel(2,2,0.25).gamestate)
                .toEqual(gamestate.ACTIVE);
        });
    });

    describe('generation', () => {
        it('generates the correct percentage of coppers', () => {
            expect(board.getElements()
                   .filter(c => c.isCop).length)
                .toEqual(1);
        });

        it('constructs the correct number of valid elements', () => {
            let w = 2, h = 2;
            let board = new BoardModel(w,h,0.25);

            expect(board.getElements().length)
                .toEqual(w * h);

            board.getElements().forEach(c => {
                expect(c).toBeDefined();
                expect(Object.values(visibility))
                    .toContain(c.visibility);
            });
        });
    });

    describe('parsing', () => {
        it('can cirleparse', () => {
            let b = new BoardModel(5, 5, 0.1);

            expect(b.getPrettyGrid())
                .toEqual(BoardModel.fromStrings(b.getPrettyGrid()).getPrettyGrid());
        });

        it('disregards badly formatted input', () => {
            expect(() => BoardModel.fromStrings(["CF", "CF CF"]))
                .toThrow();
        });
    });

    describe('popping and flagging', () => {
        it('flags a cell', () => {
            let board = BoardModel.fromStrings(["C£ /£",
                                           "/£ /£"]);

            board.flag(0,0);

            expect(board.getPrettyGrid())
                .toEqual(["CF 1£",
                          "1£ 1£"]);
        });

        it('can pop recursively', () => {
            let board = BoardModel.fromStrings(
                ["C£, /£, /£",
                 "/£, /£, /£",
                 "/£, /£, /£"
                ]);

            board.pop(2,2);

            let expected = BoardModel.fromStrings(
                ["C£, 1_, /_",
                 "1_, 1_, /_",
                 "/_, /_, /_"]
            );

            expect(board.getPrettyGrid())
                .toEqual(expected.getPrettyGrid());
        });

        it('can trigger a recursive trigger to pop on a adjacent pop with flags', () => {
            let board = BoardModel.fromStrings(
                ["/£ /£ /£ C£",
                 "/£ /£ /£ /£",
                 "C£ /£ /£ /£"
                ]
            );

            let expected = BoardModel.fromStrings(
                ["/_ /_ 1_ C£",
                 "1_ 1_ 1_ 1_",
                 "CF 1_ /_ /_"
                ]
            );

            board.pop(2,2);
            board.flag(2,0);
            board.pop(1,1);

            expect(board.getPrettyGrid())
                .toEqual(expected.getPrettyGrid());
        });

        it("can't make a visible field flagged", () => {
            let board = BoardModel.fromStrings(["C£ 1_",
                                                "1_ 1_"]);
            board.flag(1,1);
            expect(board.getPrettyGrid())
                .toEqual(["C£ 1_",
                          "1_ 1_"]);
        });

        it('can pop on adjacents when flagged', () => {
            let board = BoardModel.fromStrings(
                [ "CF /£ CF",
                  "1_ 2_ 1_",
                  "/_ /_ /_"
                ]);

            board.calculateNeighbours();
            board.pop(1,1);

            let expected = BoardModel.fromStrings(
                [ "CF /_ CF",
                  "1_ 2_ 1_",
                  "/_ /_ /_"
                ]);

            expect(board.getPrettyGrid())
                .toEqual(expected.getPrettyGrid());
        });

        it('will not pop adjacents when missing flags', () => {
            let board = BoardModel.fromStrings(
                [ "CF /£ C£",
                  "1_ 2_ 1_",
                  "/_ /_ /_"
                ]);

            board.calculateNeighbours();
            board.pop(1,1);

            let expected = BoardModel.fromStrings(
                [ "CF /£ C£",
                  "1_ 2_ 1_",
                  "/_ /_ /_"
                ]);

            expect(board.getPrettyGrid())
                .toEqual(expected.getPrettyGrid());
        });

    });

    describe('fetching', () => {
        it('can fetch a cell', () => {
            const b = BoardModel.fromStrings(["C£ 1_", "1_ 1_"]);

            expect(b.getCell(1,1).toString())
                .toEqual("1_");
        });

        it('throws when fetching bad cell', () => {
            const b = new BoardModel(2,2,0.25);

            expect(() => b.getCell(3,2)).toThrow();
        });
    });
});
