import CellModel, {visibility} from './CellModel.js';

const BoardDefaults = {
    copperPercentage: 0.1,
    boardWidth: 10,
    boardHeight: 20
};

const gamestate = {
    ACTIVE: 'active',
    WON: 'won',
    LOST: 'lost',
    INACTIVE: 'inactive'
};

const neighbouringOffsets = [
    [-1,-1],   [-1, 0],  [-1, 1],
    [ 0,-1], /*no zero*/ [ 0, 1],
    [ 1,-1],   [ 1, 0],  [ 1, 1]
];


/**
 * Gets existing neighbours of a certain cell
 */
function getNeighbours(board, x, y) {
    return neighbouringOffsets
        .map(([id, jd]) => board[x + id] === undefined
             || board[x + id][y + jd] === undefined
             ? undefined
             : {x: x + id, y: y + jd})
        .filter(c => c !== undefined);
}

/**
 * Shuffles an array and returns the shuffled one.
 */
function shuffle(origArr) {
    const arr = origArr.slice();

    for(let i = arr.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * Math.floor(i));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }

    return arr;
}

/**
 * Pop center cell and adjacents recursively
 */
function popAdjacentsRecursively(board, x, y) {
    let ps = [{x: x, y: y}];
    let bcell = pos => board[pos.x][pos.y];
    // debugger;                   
    if(board[x][y].visibility !== visibility.VISIBLE) {
        board[x][y].makeVisible();
    }

    while(ps.length > 0) {
        const p = ps.pop();

        let neighbours = getNeighbours(board, p.x, p.y)
            .filter(c => bcell(c).visibility === visibility.HIDDEN);

        neighbours.forEach(c => bcell(c).makeVisible());


        neighbours.filter(c => bcell(c).adjacents === 0)
            .map(c => ({x: c.x, y: c.y}))
            .forEach(c => ps.push(c));
    }
}


class BoardModel {
    constructor(width, height, copperPercentage) {
        this.width = width;
        this.height = height;
        this.copperPercentage = copperPercentage;
        this.clear();
    }

    getPrettyGrid() {
        return this.board
            .slice()
            .map(r => r.slice()
                 .map(c => c.toString())
                 .join(" ")
             );
    }

    static fromStrings(strs) {
        var board = new BoardModel(1,1,0.1);

        var b = [];
        for(let i in strs) {
            b[i] = [];
            let cs = strs[i].split(" ");
            for(let j in cs) {
                b[i][j] = CellModel.mkCell(cs[j], i, j);
            }
        }

        if(b.length > 0 && !b.every(br => br.length === b[0].length)) {
            throw new Error("Board has rows of different lengths");
        }

        board.board = b;
        board.calculateNeighbours();

        return board;
    }

    /**
     * Calculate the amount of dangerous tiles next to the tiles
     * and writes them to the board
     */
    calculateNeighbours() {
        for(let i = 0; i < this.board.length; i++) {
            for(let j = 0; j < this.board[i].length; j++) {
                let c = this.getCell(i, j);

                if(!c.isCop) {
                    c.adjacents = getNeighbours(this.board, i, j)
                        .filter(p => this.fromPos(p).isCop)
                        .length;
                }
            }
        }
    }

    /**
     * Generates a shuffled board.
     */
    generateBoard(w, h, copperPercentage) {
        const noCoppers = Math.ceil(copperPercentage * w * h);
        const board = new Array(h);

        const clearCells = Array(w * h - noCoppers).fill(false);
        const copCells = new Array(noCoppers).fill(true);
        const elements = shuffle(clearCells.concat(copCells));

        for(let i = 0; i < elements.length / w; i++) {
            board[i] = elements.slice(i * w, (i+1) * w);
            for(let j in board[i]) {
                board[i][j] = new CellModel(board[i][j], i, j);
            }
        }

        this.board = board;

        this.calculateNeighbours();
    }


    flag(x, y) {
        const c = this.getCell(x,y);
        if(c.visibility !== visibility.VISIBLE) {
            c.toggleFlag();
        }
    }

    getElements() {
        return this.board.reduce((acc, val) => acc.concat(val), []);
    }

    get gamestate() {
        let allClear = true;
        let lost = false;

        this.getElements().forEach(c => {
            if(!c.isCop && c.visibility !== visibility.VISIBLE) {
                allClear = false;
            }

            if(c.isCop && c.visibility === visibility.VISIBLE) {
                lost = true;
                return;
            }
        });

        if(lost)
            return gamestate.LOST;
        if(allClear)
            return gamestate.WON;
        return gamestate.ACTIVE;
    }

    clear() {
        this.generateBoard(
            this.width,
            this.height,
            this.copperPercentage
        );
    }

    fromPos(p) {
        return this.getCell(p.x, p.y);
    }

    getCell(x, y) {
        const cell = this.board[x] === undefined
              ? undefined
              : this.board[x][y];

        if(cell !== undefined){
            return cell;
        } else {
            throw new Error("No cell found at (x, y) = (" + x + ", " + y + ")");
        }
    }

    getGrid() {
        return this.board.slice();
    }

    getNeighbours(x,y) {
        return getNeighbours(this.board, x, y).map(([x,y]) => this.board[x][y]);
    }
    flaggedNeighbours(c) {
        return this.getNeighbours(c.pos.x, c.pos.y)
            .filter(c.visibility === visibility.FLAG);
    }

    pop(x,y) {
        const c = this.getCell(x,y);

        if(c.isCop) {
            c.makeVisible();
        } else if(c.adjacents === 0
                  || (c.visibility === visibility.VISIBLE
                      && getNeighbours(this.board, x, y)
                         .filter(p => this.fromPos(p).visibility === visibility.FLAG)
                         .length === c.adjacents
                     )
                 ) {
            popAdjacentsRecursively(this.board, x, y);
        } else {
            c.makeVisible();
        }


    }
}

export {BoardDefaults, gamestate};
export default BoardModel;
