const Visibility = {
    HIDDEN:  "H",
    VISIBLE: "V",
    FLAG:    "F"
};

const visibility = {
    HIDDEN: 'H',
    VISIBLE: 'V',
    FLAG: 'F'
};

class CellModel {
    constructor(isCop, x, y) {
        this._isCop = isCop || false;
        this.pos = {x: x, y: y};
        this._adjacents = undefined;
        this._visibility = visibility.HIDDEN;
    }

    set pos(o) {
        for(let i in o) {
            if(isNaN(o[i]) || o[i] < 0) {
                throw new Error(i + " value is not a number or less than zero: '"+ o[i] +"'");
            }
        }

        this._pos = {
            x: parseInt(o.x),
            y: parseInt(o.y)
        };
    }

    get text() {
        if(this.visibility === visibility.VISIBLE
           && this.adjacents > 0) {
            return this.adjacents.toString();
        } else if(this.visibility === visibility.FLAG) {
            return "F";
        } else {
            return "";
        }
    }


    toString() {
        let val = "";
        let vis = "";
        if(this.isCop) {
            val = "C";
        } else if(this.adjacents > 0) {
            val = this.adjacents;
        } else {
            val = "/";
        }

        switch(this._visibility) {
        case visibility.HIDDEN:
            vis = "£";
            break;
        case visibility.VISIBLE:
            vis = "_";
            break;
        case visibility.FLAG:
            vis = "F";
            break;
        default:
            throw new Error("Invalid visibility value: " + this._visibility);
        }

        return val + vis;
    }

    static mkCell(str, x, y) {
        let c = undefined;
        switch(str[0]) {
        case 'C':
            c = new CellModel(true, x, y);
            break;
        case '/':
            c = new CellModel(false, x, y);
            break;
        default:
            c = new CellModel(false, x, y);
            c.adjacents = parseInt(str[0]);
            break;
        }

        if(str[1] === 'F') {
            c.toggleFlag();
        } else if(str[1] === '_') {
            c.makeVisible();
        }

        return c;
    }

    get pos() {
        return this._pos;
    }

    get isCop() {
        return this._isCop;
    }

    set adjacents(a) {
        if(this._isCop) {
            throw new Error("A cop doesn't know about adjacent cops!");
        }
        if(a >= 0 && a <= 8) {
            this._adjacents = a;
        } else {
            throw new Error(a + " is an invalid number of adjacent cops!"
                            + " Should be between 1 and 8 (inclusive)");
        }
    }

    get adjacents() {
        return this._adjacents;
    }

    get visibility() {
        return this._visibility;
    }

    makeVisible() {
        switch(this._visibility) {
        case visibility.VISIBLE:
             console.debug("Cell is already visible, making it visible again is a no-op...", this.pos);
            // fallthrough
        case visibility.HIDDEN:
            this._visibility = visibility.VISIBLE;
            return;
        case visibility.FLAG:
            return;
        default:
            throw new Error("Unrecognized visibility value");
        }
    }

    toggleFlag() {
        switch(this._visibility) {
        default:
        case visibility.VISIBLE:
            throw new Error("Can only toggle flags on flags and hidden cells");

        case visibility.HIDDEN:
            this._visibility = visibility.FLAG;
            return;

        case visibility.FLAG:
            this._visibility = visibility.HIDDEN;
            return;
        }
    }

    get className() {
        if(this.visibility === visibility.HIDDEN) {
            return "cell-hidden";
        }

        if(this.visibility === visibility.FLAG) {
            return "cell-flag";
        }

        if(this.isCop) {
            return "cell-copper";
        }

        return "cell-nothing"
            + (this.adjacents !== undefined && this.adjacents > 0
               ? " cell-adjacents-" + this.adjacents
               : "");
    }
}

export {Visibility, visibility};
export default CellModel;
