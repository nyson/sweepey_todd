import React from 'react';
import './Cell.css';

class Cell extends React.Component {
    render() {
        return (
            <td className={"cell " + this.props.className}
                onClick={e => this.clickCell(e)}
                title={"x: " + this.props.posX + ", y: " + this.props.posY}
                onContextMenu={e => this.flagCell(e)}
            >
              {this.props.text}
            </td>
        );
    }

    clickCell(e) {
        this.props.onCellClick(this.props.posX, this.props.posY);
    }

    flagCell(e) {
        e.preventDefault();
        this.props.handleFlag(this.props.posX, this.props.posY);
    }
}


export default Cell;
